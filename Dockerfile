﻿FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

COPY . /App
WORKDIR /App

RUN dotnet publish src/HdoToMqtt/HdoToMqtt.csproj

FROM mcr.microsoft.com/dotnet/runtime:6.0

ENV CHROMIUM_PATH="/usr/bin/chromium"

COPY --from=build /App/src/HdoToMqtt/bin/Debug/net6.0/publish /App
WORKDIR /App

RUN apt update && \
	apt install -y chromium

ENTRYPOINT ["dotnet"]
CMD ["/App/DKX.PRE.HdoToMqtt.dll"]
