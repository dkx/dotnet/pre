﻿namespace DKX.PRE.Client.Data;

public sealed record Hdo
{
	private readonly IReadOnlyCollection<HdoItem> _items;

	public Hdo(IReadOnlyCollection<HdoItem> items)
	{
		_items = items;
	}

	public bool TryGetActiveRange(DateTime now, out HdoItem range)
	{
		HdoItem? last = null;
		foreach (var item in _items)
		{
			if (item.From > now)
			{
				break;
			}

			last = item;
		}

		if (last is null)
		{
			range = default!;
			return false;
		}

		range = last;
		return true;
	}

	public bool TryGetNextRange(HdoItem current, out HdoItem next)
	{
		var found = false;
		foreach (var item in _items)
		{
			if (found)
			{
				next = item;
				return true;
			}

			if (item == current)
			{
				found = true;
			}
		}

		next = default!;
		return false;
	}
}
