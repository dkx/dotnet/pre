﻿namespace DKX.PRE.Client.Data;

public sealed record HdoItem(DateTime From, Rate Rate);
