﻿using DKX.PRE.Client.Data;
using PuppeteerSharp;

namespace DKX.PRE.Client;

public sealed class PreClient
{
	private readonly string? _chromiumPath;

	public PreClient(string? chromiumPath = null)
	{
		_chromiumPath = chromiumPath;
	}

	public async Task<Hdo> GetHdo(string command, DateOnly fromDay, DateOnly toDay, CancellationToken cancellationToken)
	{
		if (fromDay > toDay)
		{
			throw new ArgumentException("fromDay must be greater than or equal than toDay");
		}

		var url = $"https://www.predistribuce.cz/cs/potrebuji-zaridit/zakaznici/stav-hdo/?povel={command}&den_od={fromDay.Day}&mesic_od={fromDay.Month}&rok_od={fromDay.Year}&den_do={toDay.Day}&mesic_do={toDay.Month}&rok_do={toDay.Year}";

		await using var browser = await Puppeteer.LaunchAsync(new LaunchOptions
		{
			ExecutablePath = _chromiumPath,
			Devtools = true,
			Headless = true,
			Args = new []
			{
				"--disable-gpu",
				"--disable-dev-shm-usage",
				"--disable-setuid-sandbox",
				"--no-sandbox",
			},
		});

		await using var page = await browser.NewPageAsync();
		await page.SetUserAgentAsync("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
		await page.GoToAsync(url);
		await page.WaitForSelectorAsync(".hdo-bar", new WaitForSelectorOptions { Timeout = null });

		var jsItems = await page.EvaluateFunctionAsync<HdoBarItem[][]>(@"
function() {
	const bars = document.querySelectorAll('#component-hdo-vice-dni-url .hdo-bar');
	const result = [];

	for (const bar of bars) {
		const items = [];
		let currentRate = null;

		for (const child of bar.children) {
			if (child.nodeName.toUpperCase() !== 'SPAN') {
				continue;
			}

			if (child.classList.contains('hdont')) {
				currentRate = -1
			} else if (child.classList.contains('hdovt')) {
				currentRate = 1;
			} else if (child.classList.contains('span-overflow')) {
				if (currentRate === null) {
					throw new Error('Unknown kind');
				}

				items.push({ 'Time': child.getAttribute('title').split('-')[0].trim(), 'Rate': currentRate });
				currentRate = null;
			}
		}

		result.push(items);
	}

	return result;
}
");

		await browser.CloseAsync();

		var currentDay = fromDay;
		var items = new List<HdoItem>();
		foreach (var day in jsItems)
		{
			foreach (var jsItem in day)
			{
				var rate = jsItem.Rate switch
				{
					-1 => Rate.Low,
					1 => Rate.High,
					_ => throw new ArgumentOutOfRangeException(),
				};

				// Skip if current rate is same as previous rate - new day
				if (items.Count > 0 && items.Last().Rate == rate)
				{
					continue;
				}

				var time = TimeOnly.Parse(jsItem.Time);
				var item = new HdoItem(currentDay.ToDateTime(time), rate);

				items.Add(item);
			}

			currentDay = currentDay.AddDays(1);
		}

		return new Hdo(items);
	}

	private sealed class HdoBarItem
	{
		public string Time { get; set; } = default!;
		public int Rate { get; set; }
	}
}
