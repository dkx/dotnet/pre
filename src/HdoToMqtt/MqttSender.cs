﻿using System.Text.Json;
using MQTTnet;
using MQTTnet.Client;

namespace DKX.PRE.HdoToMqtt;

internal sealed class MqttSender
{
	private readonly IMqttClient _mqtt;

	public MqttSender(IMqttClient mqtt)
	{
		_mqtt = mqtt;
	}

	public async Task ConfigureAutoDiscovery(bool remove, CancellationToken cancellationToken)
	{
		var config = new MqttApplicationMessageBuilder()
			.WithTopic("homeassistant/sensor/electricity_current_price/config")
			.WithRetainFlag();

		if (remove)
		{
			config = config.WithPayload("");
		}
		else
		{
			config = config.WithPayload(JsonSerializer.Serialize(new Dictionary<string, object>
			{
				["device_class"] = "monetary",
				["icon"] = "mdi:cash-multiple",
				["name"] = "ElectricityCurrentPrice",
				["state_class"] = "measurement",
				["state_topic"] = "homeassistant/sensor/electricity_current_price/state",
				["value_template"] = "{{ value_json.currentPrice }}",
				["json_attributes_topic"] = "homeassistant/sensor/electricity_current_price/state",
				["unit_of_measurement"] = "CZK/kWh",
				["unique_id"] = "pre-electricity_current_price",
			}));
		}

		await _mqtt.PublishAsync(config.Build(), cancellationToken);
	}

	public async Task SendState(MqttState state, CancellationToken cancellationToken)
	{
		var message = new MqttApplicationMessageBuilder()
			.WithTopic("homeassistant/sensor/electricity_current_price/state")
			.WithPayload(JsonSerializer.Serialize(state, new JsonSerializerOptions
			{
				PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
			}))
			.Build();

		await _mqtt.PublishAsync(message, cancellationToken);
	}
}
