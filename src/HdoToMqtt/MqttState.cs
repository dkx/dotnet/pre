﻿namespace DKX.PRE.HdoToMqtt;

internal sealed record MqttState(
	string CurrentPrice,
	DateTime CurrentFrom,
	DateTime CurrentTo,
	DateTime NextFrom
);
