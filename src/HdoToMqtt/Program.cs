﻿using DKX.NMoney;
using DKX.NMoney.Parser;
using DKX.PRE.Client;
using DKX.PRE.Client.Data;
using DKX.PRE.HdoToMqtt;
using MQTTnet;
using MQTTnet.Client;
using PuppeteerSharp;

var moneyParser = new DecimalParser();
var currency = new Currency("CZK");

var command = Utils.RequireEnvironmentVariable("COMMAND");
var rateLowPrice = moneyParser.Parse(Utils.RequireEnvironmentVariable("RATE_LOW_PRICE"), currency);
var rateHighPrice = moneyParser.Parse(Utils.RequireEnvironmentVariable("RATE_HIGH_PRICE"), currency);
var dry = Environment.GetEnvironmentVariable("DRY") == "1";

var mqttServer = Utils.RequireEnvironmentVariable("MQTT_SERVER");
var mqttUser = Utils.RequireEnvironmentVariable("MQTT_USER");
var mqttPassword = Utils.RequireEnvironmentVariable("MQTT_PASSWORD");


var mqttFactory = new MqttFactory();
using var mqttClient = mqttFactory.CreateMqttClient();
var mqttClientOptions = new MqttClientOptionsBuilder()
	.WithTcpServer(mqttServer)
	.WithCredentials(mqttUser, mqttPassword)
	.Build();

var cancellationTokenSource = new CancellationTokenSource();
var cancellationToken = cancellationTokenSource.Token;
Console.CancelKeyPress += (_, eventArgs) =>
{
	Console.WriteLine("Cancelling...");
	cancellationTokenSource.Cancel();
	eventArgs.Cancel = true;
};

var chromiumPath = Environment.GetEnvironmentVariable("CHROMIUM_PATH");
if (chromiumPath is null)
{
	using var browserFetcher = new BrowserFetcher();

	Console.Write("Downloading browser...");
	await browserFetcher.DownloadAsync();
	Console.WriteLine(" done");
}

var mqttConnected = await mqttClient.ConnectAsync(mqttClientOptions, CancellationToken.None);
if (!mqttClient.IsConnected)
{
	Console.WriteLine();
	Console.WriteLine($"Could not connect to MQTT ({mqttConnected.ResultCode}): {mqttConnected.ReasonString}");
	return;
}

var pre = new PreClient(chromiumPath);
var mqtt = new MqttSender(mqttClient);
var runner = new Runner(pre, mqtt, command, dry, new Dictionary<Rate, Money>
{
	[Rate.Low] = rateLowPrice,
	[Rate.High] = rateHighPrice,
});

await runner.Run(cancellationToken);
