﻿using System.Diagnostics;
using System.Globalization;
using DKX.NMoney;
using DKX.PRE.Client;
using DKX.PRE.Client.Data;

namespace DKX.PRE.HdoToMqtt;

internal sealed class Runner
{
	private readonly PreClient _pre;

	private readonly MqttSender _mqtt;

	private readonly string _command;

	private readonly bool _dry;

	private readonly IReadOnlyDictionary<Rate, Money> _price;

	private Hdo? _hdo;

	private DateTime? _hdoCreatedAt;

	public Runner(PreClient pre, MqttSender mqtt, string command, bool dry, IReadOnlyDictionary<Rate, Money> price)
	{
		_pre = pre;
		_mqtt = mqtt;
		_command = command;
		_dry = dry;
		_price = price;
	}

	public async Task Run(CancellationToken cancellationToken)
	{
		if (!_dry)
		{
			Console.Write("Configuring discovery...");
			await _mqtt.ConfigureAutoDiscovery(false, cancellationToken);
			Console.WriteLine(" done");
		}

		var timer = new PeriodicTimer(TimeSpan.FromMinutes(1));
		while (!cancellationToken.IsCancellationRequested && await timer.WaitForNextTickAsync(cancellationToken))
		{
			var watch = Stopwatch.StartNew();
			var now = DateTime.Now;

			Console.Write($"{now}: fetching HDO data...");

			var hdo = await GetHdo(now, cancellationToken);

			if (!hdo.TryGetActiveRange(now, out var range) || !hdo.TryGetNextRange(range, out var next) || !hdo.TryGetNextRange(next, out var next2))
			{
				// should not happen
				continue;
			}

			var price = _price[range.Rate];

			if (!_dry)
			{
				var state = new MqttState(price.ToDecimal().ToString(CultureInfo.InvariantCulture), range.From, next.From, next2.From);
				await _mqtt.SendState(state, cancellationToken);
			}

			Console.WriteLine($" {price.ToDecimal().ToString(CultureInfo.InvariantCulture)}CZK (took {watch.ElapsedMilliseconds}ms)");
		}
	}

	private async ValueTask<Hdo> GetHdo(DateTime now, CancellationToken cancellationToken)
	{
		if (_hdoCreatedAt is not null && _hdoCreatedAt < now.Subtract(TimeSpan.FromHours(3)))
		{
			_hdoCreatedAt = null;
		}

		if (_hdoCreatedAt is null || _hdo is null)
		{
			var from = DateOnly.FromDateTime(now);
			var to = from.AddDays(5);

			_hdo = await _pre.GetHdo(_command, from, to, cancellationToken);
			_hdoCreatedAt = now;
		}

		return _hdo;
	}
}
