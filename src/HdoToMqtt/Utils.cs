﻿namespace DKX.PRE.HdoToMqtt;

internal static class Utils
{
	public static string RequireEnvironmentVariable(string name)
	{
		var value = Environment.GetEnvironmentVariable(name)?.Trim();
		if (string.IsNullOrEmpty(value))
		{
			throw new Exception($"Missing environment variable '{name}'");
		}

		return value;
	}
}
